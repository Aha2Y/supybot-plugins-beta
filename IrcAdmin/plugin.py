###
# Copyright (c) 2012, Aha2Y
# All rights reserved.
#
#
###

import supybot.utils as utils
from supybot.commands import *
import supybot.ircdb as ircdb
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
import supybot.ircmsgs as ircmsgs

from supybot.i18n import PluginInternationalization, internationalizeDocstring

_ = PluginInternationalization('IrcAdmin')

@internationalizeDocstring
class IrcAdmin(callbacks.Plugin):
    """Add the help for "@plugin help X" here
    This should describe *how* to use this plugin."""
    threaded = True

    global logchan
    global debugchan

    def doNotice(self, irc, msg):
        if irc.network == "IotaIRC":
            logchan = self.registryValue("logchan")
            debugchan = self.registryValue('debugchan')
            print msg.args[1]
            snotice = msg.args[1].split()
            irc.sendMsg(ircmsgs.privmsg(debugchan, "%s" % snotice))
            
            if "*** Notice -- Client connecting" in msg.args[1]:
                if snotice[5] == "at":
                    msg = "14Client:7 {0} 14connected to:7 {1} 14with hostname:7 {2}"
                    irc.sendMsg(ircmsgs.privmsg(logchan, "%s" % msg.format(snotice[7], snotice[6].strip(":"), snotice[8].strip("()"))))
                    
                elif snotice[5] == "on":
                    msg = "14Client:7 {0} 14connected to:7 limonade.iotairc.net 14with hostname:7 {1}"
                    irc.sendMsg(ircmsgs.privmsg(logchan, "%s" % msg.format(snotice[8], snotice[9].strip("()"))))
                    
                if "mibbot_stats (Mibbot@bot.search.mibbit.com)" in snotice: 
                    irc.sendMsg(ircmsgs.privmsg(logchan, "14[7Client info14] 7Mibbit bot 14All it does is collecting info. (DO NOT TOUCH IT!)"))
                    
                if "(~scrawl@data.searchirc.org)" in snotice: 
                    irc.sendMsg(ircmsgs.privmsg(logchan, "14[7Client info14] 7Searchirc bot 14All it does is collecting info. (DO NOT TOUCH IT!)"))
                
            elif "*** Notice -- Client exiting" in msg.args[1]:              
                msg = "14Client:7 {0} 14disconnected:7 {1}"  
                if snotice[4] == "exiting":
                    user = snotice[7].split("!", 1)
                    irc.sendMsg(ircmsgs.privmsg(logchan, "%s" % msg.format(user[0], " ".join(snotice[8:])[1:-1])))
                    
                if snotice[4] == "exiting:":
                    irc.sendMsg(ircmsgs.privmsg(logchan, "%s" % msg.format(snotice[5], " ".join(snotice[7:])[1:-1])))
                
        elif "*** Notice -- (\x02link\x02) Link" in msg.args[1]:   
            irc.sendMsg(ircmsgs.privmsg(logchan, "%s" % msg.args[1]))
                        
    def kill(self, irc, msg, args, victim, reason="No reason."):
        """<victim> <reason>"""
        if ircdb.checkCapability(msg.nick, "oper") == True:
            irc.sendMsg(ircmsgs.IrcMsg(command="KILL", args=[victim, reason]))
        else:
            irc.errorNoCapability("oper")
    kill = wrap(kill, ["something", optional("text")])
    

    
    def broadcast(self, irc, msg, args, text):
        """<message>"""
        if ircdb.checkCapability(msg.nick, "oper") == True:
            irc.sendMsg(ircmsgs.privmsg("OperServ", "global %s" % text))
        else:
            irc.errorNoCapability("oper")
    broadcast = wrap(broadcast, ["text"])
    
Class = IrcAdmin


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
