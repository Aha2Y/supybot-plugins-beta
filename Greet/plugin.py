###
# Copyright (c) 2012, Aha2Y
# All rights reserved.
#
#
###

from BeautifulSoup import BeautifulSoup
import urllib2
import subprocess
import os
import json
import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
from BeautifulSoup import BeautifulSoup
from supybot.i18n import PluginInternationalization, internationalizeDocstring


_ = PluginInternationalization('Greet')

f = os.path.normpath(os.path.join(os.path.dirname(__file__), 'local', 'greets.json'))
try:
    greets = json.loads(open(f, 'rw').read())
except IOError as e:
    greets = {}
    with open(f, 'w') as file: json.dump(greets, open(f, 'w'))

@internationalizeDocstring
class Greet(callbacks.Plugin):
    """Add the help for "@plugin help Greet" here
    This should describe *how* to use this plugin."""
    threaded = True
    
    def doJoin(self, irc, msg):
        try:
            if greets[msg.nick.lower()]:
                if greets[msg.nick.lower()] == "random":
                    page = urllib2.urlopen("http://www.sloganmaker.com/sloganmaker.php?user=%s" % msg.nick)
                    soup = BeautifulSoup(page)
                    slogan = soup.findAll('p')[0]
                    slogan = slogan.getText()
                    irc.reply((slogan), prefixNick=False)
                else: 
                    irc.reply(greets[msg.nick.lower()])
        except KeyError: 
            pass
    
    def set(self, irc, msg, args, text):
        """<text> - When you set it to random a random greet will apear. :)"""
        if irc.nested: return
        greets[msg.nick.lower()] = text;
        irc.reply("Your current greet is now: %s" % greets[msg.nick.lower()])
        with open(f, 'w') as file: json.dump(greets, open(f, 'w'))
    set = wrap(set, ["text"])
        
    def get(self, irc, msg, args):
        """Returns your greet if you have one."""
        try:
            irc.reply("Your greet is: %s" % greets[msg.nick.lower()])
        except KeyError: 
            irc.error(_("You don't have a greet"), Raise=True)
    get = wrap(get)
        
    def unset(self, irc, msg, args):
        """Remove your current greet"""
        try:
            del greets[msg.nick.lower()]
            with open(f, 'w') as file: json.dump(greets, open(f, 'w'))
            irc.reply("Greet removed")
        except KeyError:
            irc.error(_("You don't have a greet"), Raise=True)
    unset = wrap(unset)


    
Class = Greet


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
