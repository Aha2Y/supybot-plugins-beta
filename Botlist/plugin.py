###
# Copyright (c) 2012, Aha2Y
# All rights reserved.
#
#
###
import os
import json
import supybot.ircdb as ircdb
import supybot.conf as conf
import supybot.registry as registry
import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
from supybot.i18n import PluginInternationalization, internationalizeDocstring

_ = PluginInternationalization('Botlist')

@internationalizeDocstring
class Botlist(callbacks.Plugin):
    """Add the help for "@plugin help Botlist" here
    This should describe *how* to use this plugin."""
    threaded = True

    f = os.path.normpath(os.path.join(os.path.dirname(__file__), 'local', 'bots.json'))
    global bots
    try:
        bots = json.loads(open(f, 'rw').read())
    except IOError as e:
        bots = {}
        with open(f, 'w') as file: json.dump(bots, open(f, 'w'))
    
    def add(self, irc, msg, args, message):
        """<bot name> <bot owner> - Adds the bot to the botlist."""
        if irc.nested: return
        if ircdb.checkCapability(msg.nick, "bot") == True:
            raw = msg.args[1].split()
            try:
                bots[raw[2]] = raw[3];
                with open(f, 'w') as file: json.dump(bots, open(f, 'w'))
                irc.reply("Added bot: %s with owner %s" % (raw[2], raw[3]))
            except IndexError:
                irc.reply("Syntax: add <bot name> <bot owner>")
        else:
            irc.errorNoCapability("bot")
    add = wrap(add, ["text"])
    
    def get(self, irc, msg, args, message):
        """Returns the owner if the bot is on the list."""
        if irc.nested: return
        if ircdb.checkCapability(msg.nick, "bot") == True:
            raw = msg.args[1].split()
            try:
                irc.reply("%s is the owner of %s" % (bots[raw[2]], raw[2]))
            except KeyError: 
                irc.reply("Unknown bot.")
        else:
            irc.errorNoCapability("bot")
    get = wrap(get, ["text"])
    
    def remove(self, irc, msg, args, message):
        """<bot name> <bot owner> - Removes the bot of the botlist."""
        if irc.nested: return
        if ircdb.checkCapability(msg.nick, "bot") == True:
            raw = msg.args[1].split()
            try:
                 del bots[raw[2]]
                 with open(f, 'w') as file: json.dump(bots, open(f, 'w'))
                 irc.reply("Removed bot: %s" % raw[2])
            except IndexError:
                irc.reply("Syntax: remove <bot name>")
            except KeyError: 
                irc.reply("Unknown bot.")
        else:
            irc.errorNoCapability("bot")
    remove = wrap(remove, ["text"])

Class = Botlist


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
